<!DOCTYPE html>
<html>
<head>
	<title>Data Mhs</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
     crossorigin="anonymous">
</header>
</head>
<body>

	<div class="container ">
        <h2 class="mb-0 text-uppercase custom-title ft-wt-400">Data Mahasiswa</h2>

	<a href="/tambah"> + Tambah Mahasiswa Baru</a>
    <table class="table">
        <thead>
          <tr>
            
            <th scope="col">NAMA</th>
            <th scope="col">NIM</th>
            <th scope="col">KELAS</th>
            <th scope="col">PRODI</th>
            <th scope="col">FAKULTAS</th>
            <th scope="col">OPSI</th>
          </tr>
        </thead>
        <tbody>
          <tr>

            @foreach($mahasiswa as $p)
            <tr>
                
                <td>{{ $p->nama_mahasiswa }}</td>
                <td>{{ $p->nim_mahasiswa }}</td>
                <td>{{ $p->kelas_mahasiswa }}</td>
                <td>{{ $p->prodi_mahasiswa }}</td>
                <td>{{ $p->fakultas_mahasiswa }}</td>
                <td>
                    <a href="/edit/{{ $p->id }}">Edit</a>
                    |
                    <a href="/hapus/{{ $p->id }}">Hapus</a>
                </td>
            </tr>
            @endforeach
      </table>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
 crossorigin="anonymous"></script>
</html>